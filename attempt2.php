<?php
require 'vendor/autoload.php';
require 'App.php';
require 'TestJob.php';

use Illuminate\Bus\BusServiceProvider;
use Illuminate\Events\EventServiceProvider;
use Illuminate\Foundation\Application;
use Illuminate\Queue\QueueServiceProvider;
use Illuminate\Redis\RedisManager;

$app = (new Application(dirname(__DIR__)))->useAppPath('app/App');
$container = App::getInstance();
$container->bind('redis', function () use ($container) {
    return new RedisManager($container, 'phpredis', [
        'default' => [
            'host' => 'redis_backend',
            'password' => null,
            'port' => 6379,
            'database' => 0,
        ],
    ]);
});

(new EventServiceProvider($container))->register();
(new QueueServiceProvider($container))->register();
(new BusServiceProvider($container))->register();
$container['queue']->addConnection('redis');
$container['queue']->setDefaultDriver('redis');
TestJob::dispatch();

