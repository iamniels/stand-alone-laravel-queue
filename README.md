# Stand-alone Laravel queue
Use the Laravel queue in non-Laravel applications. This is not a minimalist approach, so it's okay to use several 
Laravel service providers. However the default Laravel config files cannot be used, because the application has its own
config repository.

## Getting started
```
composer install
start a redis server on localhost
php attempt1.php
php attempt2.php
```

## The challenge
The [Torch project shows how the queue can be used](https://github.com/mattstauffer/Torch/tree/master/components/queue), 
but it doesn't implement the dispatcher and therefore a lot of features are missing.
