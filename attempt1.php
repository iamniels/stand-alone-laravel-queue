<?php

require 'vendor/autoload.php';
require 'App.php';
require 'TestJob.php';
require 'ExceptionHandler.php';

use Illuminate\Foundation\Application;
use Illuminate\Bus\Dispatcher as BusDispatcher;
use Illuminate\Events\EventServiceProvider;
use Illuminate\Queue\Capsule\Manager as Queue;
use Illuminate\Queue\Worker;
use Illuminate\Queue\WorkerOptions;
use Illuminate\Redis\RedisManager;

$app = (new Application(dirname(__DIR__)))->useAppPath('app/App');
$container = App::getInstance();
$container->bind('exception.handler', ExceptionHandler::class);


(new EventServiceProvider($container))->register();

$container->bind('redis', function () use ($container) {
    return new RedisManager($container, 'phpredis', [
        'default' => [
            'host' => 'localhost',
            'password' => null,
            'port' => 63079,
            'database' => 0,
        ],
    ]);
});
$queue = new Queue($container);
$queue->addConnection([
    'driver' => 'redis',
    'connection' => 'default',
    'queue' => 'default',
], 'redis');

$queue->setAsGlobal();
$queue->setDefaultDriver('redis');

$container['queue'] = $queue->getQueueManager();
$isDownForMaintenance = function () use ($container) {
    return $container->isDownForMaintenance();
};

$container->instance('Illuminate\Contracts\Bus\Dispatcher', (new BusDispatcher($container, function () use ($container) {
    return $container['queue']->connection();
})));

$container->instance('Illuminate\Contracts\Events\Dispatcher', $container['events']);

$queue = $container['queue'];
$events = $container['events'];
$handler = $container['exception.handler'];
$worker = new Worker($queue, $events, $handler, $isDownForMaintenance);
$options = new WorkerOptions();

TestJob::dispatch();

$worker->runNextJob('redis', 'default', $options);
//$worker->runNextJob('redis', 'default', $options);
