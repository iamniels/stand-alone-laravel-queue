<?php

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TestJob implements ShouldQueue
{
    use Dispatchable;

    public function handle() : void {
        echo "Test job has been executed.\n";
    }
}
